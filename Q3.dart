void main() {
  Winner c = new Winner();
  c.disp();
}

// Class
class Winner {
  String Appname = "FNB Banking App";
  String AppCategory = "Banking and Finance";
  String AppDeveloper = "First Rand Bank Limited";
  String YearWon = "2012";

// Function
  void disp() {
    print(Appname);
    print(AppCategory);
    print(AppDeveloper);
    print(YearWon);

    // part b)
    print(Appname.toUpperCase());
  }
}
